echo "Introduce una ip: "
read ip
while [[ ! $ip =~ ^((25[0-5]|2[0-4][0-9]|[01][0-9][0-9]|[0-9]{1,2})[.]){3}(25[0-5]|2[0-4][0-9]|[01][0-9][0-9]|[0-9]{1,2})$ ]]; do
    read -p "Esto no es una ip, vuelve a introducirla: " ip
done
echo "Esta ip si que funciona"